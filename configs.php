<?php
//TODO - Criar rotina para converter os diretorios e urls, removedo os ../ e colocando o caminho absoluto
define( 'PROJECT_ID' , 'TREINAMENTO');
define( 'SYSDIR_NAME', '../GOJIRA/src/system/');
require_once('constants.php');

//   @session_start();

   setlocale(LC_MONETARY, 'pt_BR');

   //Caminho absoluto para conseguir localizar as classes independente de onde esse arquivo for chamado
   $absPath    = str_replace( "\\", "/", dirname(__FILE__) ) . '/';

   //Localizando a pasta do sistema
   $dots = '';
   while( !file_exists( $absPath . $dots . SYSDIR_NAME . 'init.php' ) ) $dots .= '../';
   $systemPath = $absPath . $dots . SYSDIR_NAME;

   //Localizando a URL do sistema
   $baseURL = $_SERVER['SERVER_NAME'] . (isset( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : '');
   $baseURL = substr( $baseURL, 0, min(strrpos( $baseURL, '/' ), strlen( $baseURL )) );
   if( substr( $baseURL, -1, 1 ) != '/' ) $baseURL .= '/';
   $systemURL  = $baseURL . $dots . SYSDIR_NAME;

   //Inicia o objeto global
   $globals              = new StdClass();
   $globals->db          = new StdClass();

   
   error_reporting( E_ALL );
   $globals->db->name     = "treinamento";
   $globals->db->host     = "localhost";
   $globals->db->user     = "user";
   $globals->db->password = "user$";

   
   //Remove o cache do SMARTY
   define('TPL_CACHE',0);

   require_once( $systemPath . 'init.php' );
   
   if( empty( $class ) ) $class = $globals->cfg->getConfig( PROJECT_ID . '_ENGINE', 'DEFAULT_CLASS' , 'home' );
   if( empty( $proc  ) ) $proc  = $globals->cfg->getConfig( PROJECT_ID . '_ENGINE', 'DEFAULT_METHOD', 'index' );

   $globals->environment->accessLevel = 'ADM';
   
   
   //Renderiza o view que vem da url
   echo Engine::Render( $class, $proc, $param );