$(document).ready(function(){

   $("#fldtype").on("change",function(){
      if( $(":selected",this).attr('mask').length > 0 ){
         $("#fldmask").val( $(":selected",this).attr('mask') );
      }
   });


   $("#form_field").on("submit",function(evt){

      var ret = {msg: '', success: true};

      if( $("#fldname").val().length <= 0 ){
         ret.success = false;
         ret.msg    += "O campo precisa de uma identificação <br />";
      }

      if( $("#fldlabel").val().length <= 0 ){
         ret.success = false;
         ret.msg    += "Informe o nome do campo <br />";
      }

      if( !ret.success ){
         sendMsg(ret, '');
         return false;
      }
   });

   $(".dataTables_filter input").on("keyup",function(){

      var url = baseUrl + "project/csv_email/id/" +
                  $("#csv-generate").attr('project') + "/filter/" +
                  encodeURI($(this).val()) + "/";

      $("#csv-generate").attr("href", url);
      
   }).keyup();

});
