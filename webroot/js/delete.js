$(document).ready(function(){

   //Para ativar o botão com comando de teclado
      $("#email").keypress(function(evt){
            if( evt.keyCode == 13 ) $("#get_delete").click();
         });
         
      //Botão de deletar
      $("#get_delete").click(function(){
            var project = $("#project").val();
            var email   = $("#email").val();
            
            //Remove resultado anterior
            $(".box_result").html('');
            
            //Busca a lista de emails deletaveis
            $.post("engine.php",{'class':'home','proc':'get_email','project':project,'email':email},function(data){
                  var ret = $.parseJSON( data );
                  
                  if( ret.length > 0 ){
                  
                     res = $("<ul></ul>");
                     for( a in ret ){
                        $(res).append( '<li>' + ret[a] + '</li>' );
                     }
                  
                     $(".box_result").append(res);
                     $(".box_result").append('<p>Deseja excluir os emails listados? Essa operação não poderá ser revertida!</p>');
                     $(".box_result").append('<button type="button" class="btn btn-danger" id="delete_btn" project="' + project + '" email="' + email + '" >&nbsp;&nbsp;&nbsp; REMOVER &nbsp;&nbsp;&nbsp;</button><br /><br /><div class="clear"></div>');
                     $("#delete_btn").click(function(){
                           var project = $(this).attr('project');
                           var email   = $(this).attr('email');
                           $.post("engine.php",{'class':'home','proc':'del_email','project':project, 'email':email},function(data){
                                 alert( 'Emails deletados com sucesso!' );
                                 document.location.reload();
                              });
                        });
                  }
               });
         });
   });