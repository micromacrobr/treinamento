$(document).ready(function(){
      
      //Combo do company
      $("#company").change(function(){
         
            var company = $("#company").val();
            
            if( company == '' ){
               $("#project").html('<option value="">Todos</option>');
               console.log( 1 );
               return;
            }
            
            $.post('engine.php',{'class':'report','proc':'get_project','company':company},function(data){
                  var ret  = $.parseJSON( data );
                  var proj = $("#project");
                  proj.html('<option value="">Todos</option>');
                  for( var i in ret ){
                     proj.append( '<option value="' + ret[i].cod + '">' + ret[i].name + '</option>' );
                  }
               });
         });
      
      
      //Executa o comando
      $("#execute").click(function(){
         
            var company = $("#company").val();
            var project = $("#project").val();
            var command = $("#command").val();
            
            $.post('engine.php',{'class':'report','proc':'get_email','company':company,'project':project,'command':command},function(data){
                  var ret  = $.parseJSON( data );
                  
                  $(".email_content .box-body").html( "" )
                                               .append( '<table id="dataTable" class="table table-bordered table-striped">' +
                                                            '<thead><tr><th>Cliente</th><th>Projeto</th><th>Email</th><th>Sessão</th><th>Data Cad</th></tr></thead>' + 
                                                            '<tbody class="email_content_body"></tbody>' + 
                                                            '<tfoot><tr><th>Cliente</th><th>Projeto</th><th>Email</th><th>Sessão</th><th>Data Cad</th></tr></tfoot></table>' );
         
                  var body = $(".email_content_body");
                  
                  for( var i in ret ){
                     body.append( '<tr><td>' + ret[i].company + '</td>' + 
                                      '<td>' + ret[i].project + '</td>' +
                                      '<td>' + ret[i].email + '</td>' +
                                      '<td>' + ret[i].section + '</td>' + 
                                      '<td>' + ret[i].datecad + '</td></tr>' );
                  }
                  
                  $(".email_content").fadeIn(500);
                  $('#dataTable').dataTable({
                    "iDisplayLength": 50,
                    "aLengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todos"]],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "bStateSave": true,
                    "oLanguage": { 
                                     "sEmptyTable": "Nenhum registro encontrado",
                                     "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                     "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                     "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                     "sInfoPostFix": "",
                                     "sInfoThousands": ".",
                                     "sLengthMenu": "_MENU_ resultados por página",
                                     "sLoadingRecords": "Carregando...",
                                     "sProcessing": "Processando...",
                                     "sZeroRecords": "Nenhum registro encontrado",
                                     "sSearch": "Pesquisar",
                                     "oPaginate": {
                                         "sNext": "Próximo",
                                         "sPrevious": "Anterior",
                                         "sFirst": "Primeiro",
                                         "sLast": "Último"
                                     },
                                     "oAria": {
                                         "sSortAscending": ": Ordenar colunas de forma ascendente",
                                         "sSortDescending": ": Ordenar colunas de forma descendente"
                                     }
                                 }
                  });
               });
         });
      $(".email_content").hide();
   });