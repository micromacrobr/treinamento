$(document).ready(function(){

		
		$(".form-box #userid").focus();
		
		
		//Caixa de LOGIN
		$(".form-box #userid, .form-box #password").keydown(function(evt ){
				
				//Se precionar alguma tecla de confirmação, passa para o proximo campo
				if( evt.keyCode == 13 /*ENTER*/ ){
					$(".form-box #login_submit").click();	
               return false;
				}
            
			});
	
		
		
		$(".form-box #login_submit").click(function( evt ){
		
				evt.preventDefault();
		
				var name = $(".form-box #userid").val();
				var pwd  = $(".form-box #password").val();
				
				$.post("engine.php",{'class':'login','proc':'login','login_name':name, 'login_pwd':pwd},function(data){
						
                  var ret = $.parseJSON( data );
						if( ret.success ){
							document.location.reload();

						} else {
                     
							loginError( ret );
						}
                  
					});
			});

	});

//Oculta a msg de erro
function loginError( ret ){
	
   $(".msg-error").hide();
	$(".msg-error label i").html( "&nbsp;" + ret.msg );
   $(".msg-error").fadeIn(500);
	setTimeout( function(){ $(".msg-error").fadeOut(500); }, 3500 );
   $(ret.id).parent().addClass("has-error");
	
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
} 