# **SIstema para treinamento**

## Inicio 

Para inicial, sigam os seguintes passos:

 - Clonar para uma pasta local
 - Criar uma branch, a partir da *MASTER* com o nome do dev que está fazendo o treinamento
 - Configurar o sistema para rodar local
 - Executar as tarefas listadas abaixo
 - Ao finalizar, commitar e dar push do que foi realizado na branch com o seu próprio nome.
 - Avisar o responsável pelo treinamento para avaliar o que foi feito e dar o feedback
 - Duvidas, perguntar no grupo do whats ou diretamente ao responsável pelo treinamento

---

## Descrição

Esse é um sistema basico para treinamento que contém apenas 2 views simples. O primeiro deles é a listagem de itens da base de dados, com alguns botões que precisam ser configurados.

O segundo é o de inclusão/edição. Esse view é compartilhado com as procedures de Edição e Inclusão.

Esses views precisam estar todos funcionais, com todas features implementadas, sem erro e de facil uso pelos usuários.

As estruturas de funções já estão criadas, porem o código delas deverá ser criados.

---

## Tarefas

As tarefas listadas a seguir devem ser, preferêncialmente, executadas na ordem listada, pois as vezes uma depende da conclusão de outra, embora isso não seja uma regra e dependendo da situação, é possível executar em ordem diferente.

O objetivo é finalizar a tarefa o mais próximo possível do solicitado e de modo a ter um funcionamento do sistema fluido, estável, sem falhas e de forma a não gerar dificuldades desnecessárias aos usuários.

1. Criar a base de dados local com o arquivo **create_db.sql** localizado na raiz do projeto.
2. Configurar os arquivos **configs.php** e **.htaccess** para apontarem para a pasta system do GOJIRA, conexão correta com a base de dados e o rewrite_base. De modo que o sistema rode no seu servidor local com a base criada.
3. Listar os arquivos na tela (view index.html)
     - Criar a consulta na base de dados, dentro do model, na função **get_items**
     - No controller, na função **index**, buscar os dados do model, armazenar em um objeto e enviar para o view pelo return.
     - No view, já está criado o loop interando pelos registros, precisa apenas preencher os valores com as variáveis corretas.
     - Configurar os botões
          * Cada botão tem um link que deve seguir um formato especifico. Também é um caso apenas de preencher os dados que faltam.
4. Incluir campanha. Deve ser redirecionado para a função **edit**. Verificar se está tudo correto e renderizando o view corretamente.
5. Tela de edição. Essa parte deve conter as seguintes funcionálidades
     - O form deve vir com os campos preenchidos com os valores do registro que está sendo editado. Se for uma inclusão, devem    estar vazios.
     - O campo categoria, precisa ser montado.
          * No controller, chamar a função dentro do model responsável por trazer da base a lista de categorias.
          * No view, iterar entre as categorias e escrever os **<option>** com os resultados
          * No caso de edição, deve vir o campo correto selecionado por padrão, de acordo com o valor do registro sendo editado.
          * No controller, verificar se os dados estão vindo do formulário, se estiverem, enviar para a função **save**
6. Salvando o registro
     - Validar os campos NOT NULL que não possuem valores padrões
     - Validar se a categoria selecionada existe na base de dados
     - Se receber um arquivo de imagem, salvar ele
          * Validar formato do arquivo (aceitar só jpg, png e gif)
          * Criar um nome unico para o novo arquivo
          * Se for uma edição, armazenar o nome do arquivo antigo, para poder excluir depois de salvar o novo
          * Mover o arquivo para a pasta de destino no servidor
          * Preencher na variavel com os dados do registro atual o nome da nova imagem.
     - Em caso de inclusão, preencher os campos que precisam de um valor padrão (active, datecad)
     - Salvar o registro
          * Colocar tratamento de erro
          * Em caso de erro, informar o usuário
          * Em caso de sucesso, redirecionar a página para a tela de edição do novo registro (ou do registro sendo editado)
          * Se existir uma imagem antiga para ser apagada, apagar o arquivo (ANTES DE REDIRECIONAR)
7. Atualizar status (Alterar o campo active entre 0 ou 1).
     - Criar a função no controller
     - No model, criar o comando sql que irá atualizar o registro
     - Executar o commando
     - Retornar um sucesso (ou falha).
8. Remover campanha
     - Verificar se o registro informado existe (se o ID existe)
     - Deletar do disco a imagem da campanha
     - Deletar o registro da campanha da base
     - Retornar o sucesso (ou falha)

---

## Instruções adicionais

Sempre que finalizar uma tarefa, é aconselhavel fazer um commit e push para o armazenamento remoto, para evitar perda de dados e para facilitar se localizar depois, se precisar ver o status do arquivo antes ou depois de realizar determinada tarefa.

Em caso de dúvidas, converse com outros devs para entender como resolver. Se não resolver, fale diretamente com o responsável pelo treinamento.

Toda a programação do VIEW é feita com o **Smarty**, uma ferramenta de templates para facilitar a escrita do front-end. Na dúvida, busque pela documentação. Link abaixo

Na parte de links úteis tem alguns projetos que podem servir de referência

---

## Links úteis

[Documentação do Smarty - Em ingles](https://www.smarty.net/docs/en/)

[Documentação do PHP para referências de sintax](http://php.net/docs.php)

**Referências de código**
[Controller](https://bitbucket.org/micromacrobr/raemp-novo-institucional/src/master/www/painel/core/controller/project.php)
[Model](https://bitbucket.org/micromacrobr/raemp-novo-institucional/src/master/www/painel/core/model/project.php)